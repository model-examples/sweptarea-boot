---
title: "`r config.population.name` stock assessment on swept area method in `r config.population.area`"
author: "Developer: Piatinskii M., Report build by: `r config.report.author`"
date: 'Report build date: `r Sys.time()`'
output: 
  html_document:
    toc: true
    toc_depth: 3
    toc_float: true
    theme: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## 1. Model information
The default equation of swept area method was described by Aksutina [Aksutina, 1968]:

$$
\begin{aligned}
\overline{N} = \frac{Q*\overline{x}}{q*k}  \\
\end{aligned}
$$


Based on this euqation method was justificated to:
$$
\begin{aligned}
\overline{x_{boot}} = \frac{1}{b} * \sum_{j=1}^{b}{x_j^*}
\end{aligned}
$$
where xj* - bootstrap adjusted catch size in normal distribution approximation.

After find a viable bootstrap distribution confidential intervals calculated based on BCa approach (bias-corrected and accelerated). 

- Aksutina Z.M. Elements of the mathematical evaluation of the results of observations in biological and fisheries research. 1968.
- Efron B., Tibshirani R. J. An introduction to the bootstrap. – CRC press, 1994, pp. 325-328.


## 2. Input data
In this section raw input data are shown. Double-check procedure required before using the results.

```{r echo=F}
data %>% select(c("C", "S", "k")) %>%
  knitr::kable(.)
```

Configuration and parametrization settings:
```{r echo=FALSE}
for (var in ls())
  if (startsWith(var, "config.")) print(sprintf("%s=%s", var, environment()[[var]]))
```

## 3. Results
In this section summary results and graphs are shown.

### 3.1. Density
There is density weight/area estimate summary shown.
```{r echo=F}
print(fit$d.fit)
```

### 3.2. Biomass
Biomass result summarized in table 3.2.1.

```{r echo=F}
knitr::kable(
  data.frame(B = c(fit$B), B_LOW = c(fit$B_CI_LOW), B_HIGH = c(fit$B_CI_HIGH)),
  caption = "Table 3.2.1. Swept area biomass(B) results"
)
```

  - B = mean biomass
  - B_LOW = lower confidential interval biomass
  - B_HIGH = high confidential interval biomas


```{r echo=F, fig.caption="Fig. 3.2.1. Biomass distribution approximation"}
plot(density(fit$d.fit$t * config.area_size / config.weight_denum), main = "Biomass normal distribution approximation", xlab = "B, biomass")
abline(v = fit$B, col="gray", lty="dashed")
abline(v = fit$B_CI_LOW, col="gray", lty="dotted")
abline(v = fit$B_CI_HIGH, col="gray", lty="dotted")
```
