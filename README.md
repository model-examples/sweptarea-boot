# Площадной метод с аппроксимацией норм. распределения

Данный автоматизированный скрипт реализует задачи выполнения оценки запаса традиционным площадным методом [Аксютина, 1968] с использованием процедуры ресемплинга исходного ряда входных данных к нормальному стандарту распределения через бутстреп среднего. 

## 1. Требования
R version >= 3.6.1
Пакеты: dplyr, rmarkdown

## 2. Входные данные
Входные данные расположены в файле `input/survey.csv`. Значения столбцов:
1. square - квадрат лова (опционально, можно оставить пустым)
2. C - улов (в примере - в граммах)
3. S - площадь облова орудием лова (км^2)
4. q - коэффициент уловистости орудия лова

## 3. Параметризация
После подготовки входных данных параметризация выполняется в начале файла `run.R`:
```
config.population.name <- "Alosa immaculata"
config.population.area <- "Azov-Black sea"
config.area_size <- 36600 # define total area size square
config.remove_zeros <- TRUE # remove zero-catch stations from data? TRUE or FALSE value allowed only
config.weight_denum <- 1000000 # weight multiplier. Example for source data in grams, 1000000 will lead to result in tones
config.iter_num <- 50000 # bootstrap iteration number, recommended values: 10k - 100k
config.p_level = 0.95 # confidential level, default p = 0.95
config.report.author <- "Piatinskii M."
```

## 4. Выполнение расчетов
Выполняется при помощи исполнения файла `run.R` целиком:

```
source("run.R")
```

## 5. Результаты
После выполнения расчетов все результаты сохраняются в файле `Report.html`. 

## 6. Авторы
Пятинский М.М., Азово-Черноморский филиал "ВНИРО". 