############### Configuration area #######################
config.population.name <- "Alosa immaculata"
config.population.area <- "Azov-Black sea"
config.area_size <- 36600 # define total area size square
config.remove_zeros <- TRUE # remove zero-catch stations from data? TRUE or FALSE value allowed only
config.weight_denum <- 1000000 # weight multiplier. Example for source data in grams, 1000000 will lead to result in tones
config.iter_num <- 50000 # bootstrap iteration number, recommended values: 10k - 100k
config.p_level = 0.95 # confidential level, default p = 0.95
config.report.author <- "Piatinskii M."
############### Configuration area END ###################


# DO NOT TOUCH ANYTHING BELOW IF YOU HAVE NO IDEA WHAT IS GOING ON!!!

library("rmarkdown")
library("dplyr")
# load script
source("package.R")

# load data
data <- read.csv("input/survey.csv", sep=";", header = TRUE)
if (!("C" %in% colnames(data))) 
  stop("Column \"C\" not found in survey.csv")

if (!("S" %in% colnames(data)))
  stop("Column \"S\" not found in survey.csv")

if (!("k" %in% colnames(data)))
  stop("Column \"k\" not found in survey.csv")

if (config.remove_zeros) {
  warning("Attention! Remove zero-catch values from data seria")
  data <- data[data$C!=0,]
}


print("Start sweptarea iterations ...")
# fit model
fit <- sweptarea(C = data$C, S = data$S, k = data$k, total_space = config.area_size, dim = config.weight_denum, p = config.p_level, R = config.iter_num, plot = T)
print(paste0("Density distribution mean = ", fit$d.fit$t0))
print(paste0("Biomass = ", fit$B, ", CI = [", fit$B_CI_LOW, " - ", fit$B_CI_HIGH , "]"))
print("Calculation done!")

# generate report
render("Report.Rmd", quiet = T)
print("See full report in Report.html")